package es.upm.emse.absd;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

public final class Utils {

    private static int cidCnt = 0;
    private static String cidBase;

    public static String genCID(Agent agent)
    {
        if (cidBase==null) {
            cidBase = agent.getLocalName() + agent.hashCode() +
                System.currentTimeMillis()%10000 + "_";
        }
        return  cidBase + (cidCnt++);
    }

    public static ACLMessage newMsg(Agent origin, int perf, String content, AID dest)
    {
        ACLMessage msg = newMsg(origin, perf);
        if (dest != null) msg.addReceiver(dest);
        msg.setContent( content );
        return msg;
    }

    public static ACLMessage newMsg(Agent origin, int perf)
    {
        ACLMessage msg = new ACLMessage(perf);
        msg.setConversationId(genCID(origin));
        return msg;
    }

}
