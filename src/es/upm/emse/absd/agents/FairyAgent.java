package es.upm.emse.absd.agents;

import es.upm.emse.absd.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class FairyAgent extends Agent {

    protected void setup()
    {

        List<String> words = Arrays.asList("Hey!", "Listen!", "Hello!", "Link!");
        Random rand = new Random();

        Behaviour loop = new TickerBehaviour(this, 2500)
        {
            protected void onTick() {

                ACLMessage msg = receive();
                if (msg != null) {
                    System.out.println(this.getAgent().getLocalName() + ": HIIIIIIII!!! :D");
                    doSuspend();
                }
                else {
                    String randomWord = words.get(rand.nextInt(words.size()));
                    System.out.println(this.getAgent().getLocalName() + ": " + randomWord);
                    send(Utils.newMsg(this.getAgent(), ACLMessage.INFORM, randomWord, new AID("Link", AID.ISLOCALNAME)));
                }

            }
        };
        addBehaviour(loop);
    }

}
